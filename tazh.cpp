/*!
* ������� ����-��������� �� ������ �����
* \param[in] integer �����
* \param[in] lexPos ������� �����
* \param[out] ����-���������
*/
struct Expression* createIntExpr(int integer, YYLTYPE lexPos){
	// ��������� ������ ��� ����
	struct Expression* result = (struct Expression*)malloc(sizeof(struct Expression));
	// ��������� ���� ���������
	result->type = Int;
	result->hasOperands = 0;
	result->num = integer;
	result->lexPos = lexPos;
	// �������������� �������� ����
	// ��� ���� ��������������� �� ������������ ��� ������� ����
	// �� ����� ���� ������, ���� ���������������������
	result->localVarNum = -1;
	result->numFromConstTable = -1;
	result->isUsage = 0;
	result->leftOp = NULL;
	result->rightOp = NULL;
	result->isUsageBefore = -1;
	result->isFirstIdEntry = -1;
	// ���������� ���������� ����
	return result; 
}